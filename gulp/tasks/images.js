'use strict';

var gulp = require('gulp'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	outputDir = 'build';

gulp.task('images', function(){
  return gulp.src('src/images/**/*.+(png|jpg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
	  interlaced: true
   })))
  .pipe(gulp.dest(outputDir + '/images'))
});