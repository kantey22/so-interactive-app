'use strict';

var gulp = require('gulp'),
	connect = require('gulp-connect'),
	htmlmin = require('gulp-html-minifier'),
	fileinclude = require('gulp-file-include'),
	outputDir = 'build';

gulp.task('html', function () {
	gulp.src(['src/**/*.html'])
	.pipe(fileinclude({
      prefix: '@',
      basepath: 'src/partials/'
    }))
	.pipe(htmlmin({collapseWhitespace: true}))
	.pipe(gulp.dest(outputDir))
   	.pipe(connect.reload())
});
