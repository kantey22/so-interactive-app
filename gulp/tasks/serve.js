'use strict';

var gulp = require('gulp'),
	connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    root: 'build',
    port: 9001,
    livereload: true
  });
});