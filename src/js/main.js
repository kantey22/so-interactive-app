 $(document).ready(function () {
     $(document).on("scroll", onScroll);

     $('a[href^="#"]').on('click', function (e) {
         e.preventDefault();
         $(document).off("scroll");
         var target = this.hash;
         $target = $(target);
         $('html, body').stop().animate({
             'scrollTop': $target.offset().top + 0
         }, 500, 'swing', function () {
             window.location.hash = target;
             $(document).on("scroll", onScroll);
         });
     });
 });

 function onScroll(event) {
     var scrollPosition = $(document).scrollTop();
     $(' a').each(function () {
         var currentLink = $(this);
         var refElement = $(currentLink.attr("href"));
     });
 }

 $(window).scroll(function () {
     if ($(this).scrollTop() >= 100) { // If page is scrolled more than 50px
         $('#return-to-top').fadeIn(200); // Fade in the arrow
     } else {
         $('#return-to-top').fadeOut(200); // Else fade out the arrow
     }
 });
 $('#return-to-top').click(function () { // When arrow is clicked
     $('body,html').animate({
         scrollTop: 0 // Scroll to top of body
     }, 500);
 });